json.array!(@time_slots) do |time_slot|
  json.extract! time_slot, :id, :time_string, :time_num, :time_availability
  json.url time_slot_url(time_slot, format: :json)
end
