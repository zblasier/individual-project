json.array!(@diagnostics) do |diagnostic|
  json.extract! diagnostic, :id, :diagnostic_desc, :cost
  json.url diagnostic_url(diagnostic, format: :json)
end
