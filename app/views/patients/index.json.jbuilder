json.array!(@patients) do |patient|
  json.extract! patient, :id, :patient_name, :patient_address, :patient_city, :patient_zip, :patient_primary_phone, :patient_secondary_phone
  json.url patient_url(patient, format: :json)
end
