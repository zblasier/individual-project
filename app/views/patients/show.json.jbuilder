json.extract! @patient, :id, :patient_name, :patient_address, :patient_city, :patient_zip, :patient_primary_phone, :patient_secondary_phone, :created_at, :updated_at
