json.extract! @physician, :id, :physician_name, :physician_address, :physician_city, :physician_zip, :physician_primary_phone, :physician_secondary_phone, :physician_status, :created_at, :updated_at
