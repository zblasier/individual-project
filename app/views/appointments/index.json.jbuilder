json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :appointment_date, :time_slot_id, :appointment_reason, :physician_id, :diagnostic_id, :patient_id, :note
  json.url appointment_url(appointment, format: :json)
end
