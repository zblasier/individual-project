json.extract! @appointment, :id, :appointment_date, :time_slot_id, :appointment_reason, :physician_id, :diagnostic_id, :patient_id, :note, :created_at, :updated_at
