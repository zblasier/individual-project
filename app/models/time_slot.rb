class TimeSlot < ActiveRecord::Base
  has_many :appointments

  validates :time_string, :time_num, presence: true
end
