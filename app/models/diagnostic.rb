class Diagnostic < ActiveRecord::Base
  has_many :appointments

  validates :diagnostic_desc, :cost, presence: true
end
