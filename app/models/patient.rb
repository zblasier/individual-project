class Patient < ActiveRecord::Base
  has_many :appointments
  has_many :physicians, through: :appointments
  has_many :diagnostics, through: :appointments
  has_many :time_slots, through:  :appointments

  validates :patient_name, presence: true
  validates :patient_address, :patient_city, :patient_zip, :patient_primary_phone,  presence: true


end
