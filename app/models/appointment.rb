class Appointment < ActiveRecord::Base
  belongs_to :physician
  belongs_to :patient
  belongs_to :time_slot
  belongs_to :diagnostic

  validates_uniqueness_of :time_slot_id, :scope => [:appointment_date, :physician_id]
  validates :appointment_date, :time_slot_id, :patient_id, :physician_id, presence: true

  before_create :dt_valid, :day_check, :time_check
  before_destroy :early_cancel


  def dt_valid

        if self.appointment_date < Date.today()
      errors.add(:appointment_date, "cannot be before current time.")
        return false
        end
  end
  def early_cancel

          time_comp = self.time_slot.time_num - Time.now()
          if self.appointment_date == Date.today() and time_comp < 8
          errors.add(:time_slot, "can't cancel")

          return false
          end
  end

   def time_check
    if self.time_slot.time_num < Time.now()
      errors.add(:time_slot_id, 'You have selected a time before now')
    end
    end
  def day_check
    if appointment_date.saturday? == true or appointment_date.sunday? == true
      errors.add(:appointment_date, 'Those are weekends, please choose a weekday')
      return false
    end
  end

   end
