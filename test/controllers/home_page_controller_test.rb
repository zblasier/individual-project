require 'test_helper'

class HomePageControllerTest < ActionController::TestCase
  test "should get admin" do
    get :admin
    assert_response :success
  end

  test "should get doctor" do
    get :doctor
    assert_response :success
  end

  test "should get home" do
    get :home
    assert_response :success
  end

  test "should get office" do
    get :office
    assert_response :success
  end

  test "should get patient" do
    get :patient
    assert_response :success
  end

end
