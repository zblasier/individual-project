# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
open("/vagrant/derm_codesv2.csv")do|diagnose|
  diagnose.read.each_line do |diagnostic|
    diagnostic_desc, cost = diagnostic.chomp.split(",")
    Diagnostic.create!(:diagnostic_desc => diagnostic_desc, :cost => cost)
  end
end