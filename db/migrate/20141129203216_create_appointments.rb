class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.date :appointment_date
      t.integer :time_slot_id
      t.text :appointment_reason
      t.integer :physician_id
      t.integer :diagnostic_id
      t.integer :patient_id
      t.text :note

      t.timestamps
    end
  end
end
