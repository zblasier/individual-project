class CreateTimeSlots < ActiveRecord::Migration
  def change
    create_table :time_slots do |t|
      t.string :time_string
      t.time :time_num
      t.boolean :time_availability

      t.timestamps
    end
  end
end
