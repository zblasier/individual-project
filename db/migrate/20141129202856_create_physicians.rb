class CreatePhysicians < ActiveRecord::Migration
  def change
    create_table :physicians do |t|
      t.string :physician_name
      t.string :physician_address
      t.string :physician_city
      t.string :physician_zip
      t.string :physician_primary_phone
      t.string :physician_secondary_phone
      t.boolean :physician_status

      t.timestamps
    end
  end
end
