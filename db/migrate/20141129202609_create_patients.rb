class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :patient_name
      t.string :patient_address
      t.string :patient_city
      t.string :patient_zip
      t.string :patient_primary_phone
      t.string :patient_secondary_phone

      t.timestamps
    end
  end
end
