class CreateDiagnostics < ActiveRecord::Migration
  def change
    create_table :diagnostics do |t|
      t.string :diagnostic_desc
      t.decimal :cost

      t.timestamps
    end
  end
end
